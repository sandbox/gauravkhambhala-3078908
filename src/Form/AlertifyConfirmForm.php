<?php
namespace Drupal\alertifyjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AlertifyConfirmForm extends ConfigFormBase  {

  const SETTINGS = 'alertifyjsconfirm.settings';

  public function getFormId()
  {
      return 'alertifyconfirm_config_form';
  }

  /**
   * {@inheritdoc}
   */

  protected function getEditableConfigNames() {
    return [
        static::SETTINGS,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state){
    $config = $this->config(static::SETTINGS);
    $autoReset = $config->get('autoReset');
    $basic = $config->get('basic');
    $closable = $config->get('closable');
    $closableByDimmer = $config->get('closableByDimmer');
    $frameless = $config->get('frameless');
    $ok_text = $config->get('ok_text');
    $cancel_text = $config->get('cancel_text');
    $maximizable = $config->get('maximizable');
    $message = $config->get('message');
    $modal = $config->get('modal');
    $movable = $config->get('movable');
    $defaultFocus = $config->get('defaultFocus');
    $moveBounded = $config->get('moveBounded');
    $overflow = $config->get('overflow');
    $padding = $config->get('padding');
    $pinnable = $config->get('pinnable');
    $resizable = $config->get('resizable');
    $reverseButtons = $config->get('reverseButtons');
    $startMaximized = $config->get('startMaximized');
    $transition = $config->get('transition');
    $form['autoReset'] = [
      '#type' => 'radios',
      '#description' => $this->t('Indicating whether the dialog should reset size/position on window resize'),
      '#title' => $this->t('AutoReset'),
      '#default_value' => (isset($autoReset)) ? $autoReset : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['basic'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets the dialog basic view mode, which hides both header/footer reserving their space as not to affect other options such as resize, move, maximize...etc'),
      '#title' => $this->t('Basic'),
      '#default_value' => (isset($basic)) ? $basic : "0",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['closable'] = [
      '#type' => 'radios',
      '#description' => $this->t('sets a value indicating whether the Close button is displayed in the header of the dialog. In modal dialogs, clicking the dimmer will also trigger a close command unless closableByDimmer was set to false'),
      '#title' => $this->t('Closable'),
      '#default_value' => (isset($closable)) ? $closable : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['closableByDimmer'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether clicking the dimmer of a Closable Modal dialog should trigger a close command or not'),
      '#title' => $this->t('ClosableByDimmer'),
      '#default_value' => (isset($closableByDimmer)) ? $closableByDimmer : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['defaultFocus'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether to reverse the dialog buttons order'),
      '#title' => $this->t('DefaultFocus'),
      '#default_value' => (isset($defaultFocus)) ? $defaultFocus : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('Ok'),
        "0" => $this->t('Cancel'),
      ],
    ];
    $form['frameless'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets the dialog frameless view mode, which hides both header/footer. But footer space is not reserved, the content author must be aware that dialog header/commands might overlap with contents'),
      '#title' => $this->t('Frameless'),
      '#default_value' => (isset($frameless)) ? $frameless : "0",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['labels'] = [
      '#type' => 'details',
      '#title' => $this->t('Set the OK/Cancel button\'s label'),
      '#open' => TRUE,
    ];
    $form['labels']['ok_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ok'),
      '#description' => $this->t('Ok button text'),
      '#default_value' => (isset($ok_text)) ? $ok_text : "Ok",
      '#required' => TRUE,
    ];
    $form['labels']['cancel_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cancel'),
      '#description' => $this->t('Cancel button text'),
      '#default_value' => (isset($cancel_text)) ? $cancel_text : "Cancel",
      '#required' => TRUE,
    ];
    $form['maximizable'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the Maximize button is displayed in the header of the dialog'),
      '#title' => $this->t('Maximizable'),
      '#default_value' => (isset($maximizable)) ? $maximizable : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Sets the dialog contents'),
      '#default_value' => (isset($message)) ? $message : "Are you sure?",
      '#required' => TRUE,
    ];
    $form['modal'] = [
      '#type' => 'radios',
      '#title' => $this->t('Modal'),
      '#description' => $this->t('Sets the dialog display mode, When set to 1 a screen dimmer will be used and access to the page contents will be prevented'),
      '#default_value' => (isset($modal)) ? $modal : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['movable'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the dialog is movable'),
      '#title' => $this->t('Movable'),
      '#default_value' => (isset($movable)) ? $movable : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['moveBounded'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether a movable dialog is allowed to go off-screen or not'),
      '#title' => $this->t('MoveBounded'),
      '#default_value' => (isset($moveBounded)) ? $moveBounded : "0",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['overflow'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the content overflow is managed by the dialog or not'),
      '#title' => $this->t('Overflow'),
      '#default_value' => (isset($overflow)) ? $overflow : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['padding'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the content padding is managed by the dialog or not'),
      '#title' => $this->t('Padding'),
      '#default_value' => (isset($padding)) ? $padding : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['pinnable'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the Pin button is displayed in the header of the dialog (modeless only)'),
      '#title' => $this->t('Pinnable'),
      '#default_value' => (isset($pinnable)) ? $pinnable : "1",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['resizable'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the dialog is resizable'),
      '#title' => $this->t('Resizable'),
      '#default_value' => (isset($resizable)) ? $resizable : "0",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['reverseButtons'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether to reverse the dialog buttons order'),
      '#title' => $this->t('ReverseButtons'),
      '#default_value' => (isset($reverseButtons)) ? $reverseButtons : "0",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['startMaximized'] = [
      '#type' => 'radios',
      '#description' => $this->t('Sets a value indicating whether the dialog should start in a maximized state or not'),
      '#title' => $this->t('StartMaximized'),
      '#default_value' => (isset($startMaximized)) ? $startMaximized : "0",
      '#required' => TRUE,
      '#options' => [
        "1" => $this->t('TRUE'),
        "0" => $this->t('FALSE'),
      ],
    ];
    $form['transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Transition'),
      '#default_value' => (isset($transition)) ? $transition : "pulse",
      '#description' => $this->t('Sets the transition effect to be used when showing/hiding the dialog'),
      '#required' => TRUE,
      '#options' => [
        "fade" => $this->t('fade'),
        "flipx" => $this->t('flipx'),
        "flipy" => $this->t('flipy'),
        "pulse" => $this->t('pulse'),
        "slide" => $this->t('slide'),
        "zoom" => $this->t('zoom'),
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('autoReset', $form_state->getValue('autoReset'))
      ->set('basic', $form_state->getValue('basic'))
      ->set('closable', $form_state->getValue('closable'))
      ->set('closableByDimmer', $form_state->getValue('closableByDimmer'))
      ->set('frameless', $form_state->getValue('frameless'))
      ->set('ok_text', $form_state->getValue('ok_text'))
      ->set('cancel_text', $form_state->getValue('cancel_text'))
      ->set('maximizable', $form_state->getValue('maximizable'))
      ->set('message', $form_state->getValue('message'))
      ->set('modal', $form_state->getValue('modal'))
      ->set('movable', $form_state->getValue('movable'))
      ->set('defaultFocus', $form_state->getValue('defaultFocus'))
      ->set('moveBounded', $form_state->getValue('moveBounded'))
      ->set('overflow', $form_state->getValue('overflow'))
      ->set('padding', $form_state->getValue('padding'))
      ->set('pinnable', $form_state->getValue('pinnable'))
      ->set('resizable', $form_state->getValue('resizable'))
      ->set('reverseButtons', $form_state->getValue('reverseButtons'))
      ->set('startMaximized', $form_state->getValue('startMaximized'))
      ->set('transition', $form_state->getValue('transition'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}