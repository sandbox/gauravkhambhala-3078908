<?php
namespace Drupal\alertifyjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AlertifyForm extends ConfigFormBase  {

  const SETTINGS = 'alertifyjsmessageforms.settings';

  public function getFormId()
  {
    return 'alertify_form_message';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state){
    $config = $this->config(static::SETTINGS);
    $i = 0;
    $total_rows = count($config->get('message_fieldset'));
    if($total_rows <= 0){
      $total_rows = 1;
    }
    $fieldsets = $form_state->get('fieldset_rows');
    if (empty($fieldsets)) {
      $fieldsets = $form_state->set('fieldset_rows', $total_rows);
    }
    $fieldsets = $form_state->get('fieldset_rows');
    $form['message_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Form Information'),   
      '#prefix' => '<div class="custom-fielset" id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['#tree'] = TRUE;
    $i= 0;
    $index = 0;
    for ($i = 0; $i < $fieldsets; $i++) {
      $values = $config->get('message_fieldset');
      $form['message_fieldset'][$i]['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => $values[$i]['title'],
        '#description' => $this->t('Title to be displayed over confirmation box'),
        '#prefix' => '<div class="custom_fielset_wrapper" id="names-fieldset-wrapper">',
      ];
      $form['message_fieldset'][$i]['form_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Form ID'),
        '#default_value' => $values[$i]['form_id'],
        '#description' => 'e.g : alertifyconfirm_config_form',
      ];
      $form['message_fieldset'][$i]['message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message'),
        '#default_value' => $values[$i]['message'],
        '#description' => $this->t('Message to be displayed in confirmation box'),
      ];
      $form['message_fieldset'][$i]['remove'] = [
        '#type' => 'checkbox',
        '#title' => t('Remove'),
        '#size' => 10,
        '#maxlength' => 255,
        '#default_value' => 0,
        '#suffix' => '</div>',
        '#description' => $this->t('Click to remove this form'),
      ];
    }
    // if($i > count($fieldsets)){
    //   $form['message_fieldset'][$i-1]['title']['#required'] = FALSE;
    //   $form['message_fieldset'][$i-1]['form_id']['#required'] = FALSE;
    //   $form['message_fieldset'][$i-1]['message']['#required'] = FALSE;
    // }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => array('::addOne'),
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);    
  }

  public function addOne(array &$form, FormStateInterface $form_state) {
    $fieldsets = $form_state->get('fieldset_rows');
    $form_state->set('fieldset_rows', $fieldsets + 1);
    $form_state->setRebuild();
  }

  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $fieldsets = $form_state->get('fieldset_rows');
    return $form['message_fieldset'];
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fieldsets = $form_state->getValue(['message_fieldset']);
    $key_to_pop = '';
    foreach($fieldsets as $key => $value){
      if(empty($value['title']) || empty($value['form_id']) || empty($value['message']) || $value['remove'] == 1){
            $key_to_pop = $key;
            unset($fieldsets[$key_to_pop]);
      }
    }
    $fieldsets = array_values($fieldsets);
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('message_fieldset',$fieldsets)
      ->save();
    parent::submitForm($form, $form_state);
  }
}