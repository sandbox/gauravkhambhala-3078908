<?php
namespace Drupal\alertifyjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AlertifyNotifierForm extends ConfigFormBase  {

  const SETTINGS = 'alertifyjsnotifier.settings';

  public function getFormId()
  {
    return 'alertifynotifier_config_form';
  }

  /**
   * {@inheritdoc}
   */

  protected function getEditableConfigNames() {
    return [
        static::SETTINGS,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state){
    $config = $this->config(static::SETTINGS);
    $delay = $config->get('delay');
    $position = $config->get('position');
    $form['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay'),
      '#default_value' => (isset($delay)) ? $delay : 5,
      '#description' => $this->t('Sets a value indicating the notifier default message delay (in seconds) before being auto-dismissed. 0 will means keep open till clicked.'),
    ];
    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#description' => $this->t('Sets a value indicating the position of the notifier instance.'),
      '#default_value' => (isset($position)) ? $position : 'bottom-right',
      '#options' => [
        'top-right' => $this->t('top-right'),
        'top-center' => $this->t('top-center'),
        'top-left' => $this->t('top-left'),
        'bottom-right' => $this->t('bottom-right'),
        'bottom-center' => $this->t('bottom-center'),
        'bottom-left' => $this->t('bottom-left'),
      ]
    ];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('delay',$form_state->getValue('delay'))
      ->set('position',$form_state->getValue('position'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}