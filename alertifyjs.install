<?php

/**
 * @file
 * Implements installation hooks.
 */

 /**
 * Implements hook_requirements().
 */
function alertifyjs_requirements($phase) {
  $requirements = [];

  switch ($phase) {
    case 'install':
    case 'runtime':
      $file_exists = file_exists(DRUPAL_ROOT . '/libraries/alertifyjs/alertify.min.js');

      if ($file_exists) {
        $message = t('alertifyjs plugin detected in %path.', ['%path' => '/libraries/alertifyjs']);
      }
      else {
        $message = t('The alertifyjs plugin was not found. Please <a href=":repository_url" target="_blank">download</a> and save into the libraries folder in the root (/libraries/alertifyjs/alertify.min.js).', [':repository_url' => 'https://alertifyjs.com']);
      }

      $requirements['alertifyjs'] = [
        'title' => t('alertifyjs Plugin'),
        'description' => $message,
        'severity' => $file_exists ? REQUIREMENT_OK : REQUIREMENT_ERROR,
      ];

      break;
  }

  return $requirements;
}

 /**
 * Implements hook_install().
 */
function alertifyjs_install() {
  \Drupal::configFactory()->getEditable('alertifyjsconfirm.settings')
    ->set('autoReset',"1")
    ->set('basic',"0")
    ->set('closable',"1")
    ->set('closableByDimmer',"1")
    ->set('frameless',"0")
    ->set('ok_text',"Ok")
    ->set('cancel_text',"Cancel")
    ->set('maximizable',"1")
    ->set('message',"Are you Sure?")
    ->set('modal',"1")
    ->set('movable',"1")
    ->set('defaultFocus',"1")
    ->set('moveBounded',"0")
    ->set('overflow',"1")
    ->set('padding',"1")
    ->set('pinnable',"1")
    ->set('resizable',"0")
    ->set('reverseButtons',"0")
    ->set('startMaximized',"0")
    ->set('transition',"pulse")
    ->save();
  \Drupal::configFactory()->getEditable('alertifyjsnotifier.settings')
    ->set('delay',5)
    ->set('position',"bottom-right")
    ->save();
}

 /**
 * Implements hook_uninstall().
 */
function alertifyjs_uninstall() {
  \Drupal::configFactory()->getEditable('alertifyjsconfirm.settings')->delete();
  \Drupal::configFactory()->getEditable('alertifyjsnotifier.settings')->delete();
  \Drupal::configFactory()->getEditable('alertifyjsmessageforms.settings')->delete();
}