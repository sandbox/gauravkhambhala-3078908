CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

 * Integrates the Alertify JavaScript plug-in.
 * This is the advanced version of alertifyjs library.
 * This will convert simple js popup to modern popup.

REQUIREMENTS
------------

 * This module will depends upon alertifyjs library.
 * < https://alertifyjs.com/ >

INSTALLATION
------------

 * Alertify Depends upon alertifyjs library.
 * You have to Download the alertifyjs library.
 * < https://alertifyjs.com/ >
 * Save it to /libraries/alertifyjs.
 * After saving it download and enable the AlertifyJS module.

CONFIGURATION
-------------

 * No additional configuration required.
