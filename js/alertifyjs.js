(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.alertify_behaviour = {
    attach: function (context, settings) {
      var messages = drupalSettings.status_messages;
      var confirm_config = drupalSettings.confirm_config;
      var notify_config = drupalSettings.notify_config;
      alertify.set('notifier','position', notify_config.position);
      alertify.set('notifier','delay', notify_config.delay);

        $('body', context).once('alertify_behaviour').each(function () {
          if (messages && messages instanceof Object) {
            $.each(messages, function (key, value) {
              switch (key) {
                case 'warning':
                  alertify.warning(value[0]);
                  break;
                case 'error':
                  alertify.error(value[0]);
                  break;
                case 'status':
                default:
                  alertify.success(value[0]);
                  break;
              }
            });
          }
          if (confirm_config && confirm_config instanceof Object) {
              var config_object = {};
              var fselector = '#' + confirm_config.fid;
              $.each(confirm_config, function (key, value) {
                if (value == "0") {
                  value = false;
                } else if (value == "1") {
                  value = true;
                }
                config_object[key] = value;
              });
            var flag = false;
            alertify.confirm()
                  .setting({
                    'title': config_object.title,
                    // 'message': config_object.message,
                    'basic': config_object.basic,
                    'cancel_text': config_object.cancel_text,
                    'closable': config_object.closable,
                    'closableByDimmer': config_object.closableByDimmer,
                    'defaultFocus': config_object.defaultFocus,
                    'frameless': config_object.frameless,
                    'maximizable': config_object.maximizable,
                    'modal': config_object.modal,
                    'movable': config_object.movable,
                    'moveBounded': config_object.moveBounded,
                    'ok': config_object.ok_text,
                    'overflow': config_object.overflow,
                    'padding': config_object.padding,
                    'pinnable': config_object.pinnable,
                    'resizable': config_object.resizable,
                    'reverseButtons': config_object.reverseButtons,
                    'startMaximized': config_object.startMaximized,
                    'transition': config_object.transition,
                    'onok': 
                      function(){
                        flag = false;
                        $(fselector)[0].submit();
                        return true;
                      },
                    'oncancel':
                      function(){
                        flag = false;
                        alertify.error(config_object.cancel_text);
                        return true;
                      }
                  });
            if ($(fselector).length) {
              jQuery(fselector).on('submit', function (e) {
                e.preventDefault();
                alertify.confirm(config_object.message);
              });              
            }
          }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);